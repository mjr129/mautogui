import mautogui.editoria.qt_editorium.default_editors as d
from mautogui.editoria.qt_editorium.bases import Editorium

def create_default() -> Editorium:
    e = Editorium()
    e.editors.append( d.ReadonlyEditor )
    e.editors.append( d.PasswordEditor )
    e.editors.append( d.FilenameEditor )
    e.editors.append( d.AnnotationEditor )
    e.editors.append( d.BoolEditor )
    e.editors.append( d.StringCoercionEnumEditor )
    e.editors.append( d.FlagsEditor )
    e.editors.append( d.FloatEditor )
    e.editors.append( d.IntEditor )
    e.editors.append( d.StringEditor )
    e.editors.append( d.ListTEditor )
    e.editors.append( d.NullableEditor )
    e.editors.append( d.NoneEditor )
    e.editors.append( d.UnionEditor )
    e.editors.append( d.FallbackEditor )
    return e
