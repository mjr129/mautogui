import sys
import threading
from typing import Optional

from PyQt5.QtCore import QThread, pyqtSignal

from mautogui.editoria import qt_editorium

from mhelper import string_helper, ansi_format_helper
from mautogui.qt_autogui.forms.frm_maintenance import FrmMaintenance
from mhelper.comment_helper import override
from mhelper.mannotation import ArgsKwargs, ArgValueCollection
from .async_result import Result, TaskCancelledError


class WindowedRunner:
    def __init__( self,
                  function,
                  *,
                  parent,
                  confirm = False,
                  auto_close = False,
                  editorium: qt_editorium.Editorium,
                  user_options: dict ):
        self.function = function
        self.window = parent
        self.confirm = confirm
        self.editorium: qt_editorium.Editorium = editorium
        self.auto_close = auto_close
        self.user_options = user_options
        
        self.bee_thread = None
        self.running_dialogue: FrmMaintenance = None
    
    
    def run( self, *args, **kwargs ) -> Optional[Result]:
        args = ArgsKwargs( *args, **kwargs )
        
        if self.confirm:
            from mautogui.qt_autogui.forms.frm_arguments import FrmArguments
            avc: ArgValueCollection = FrmArguments.query( owner_window = self.window,
                                                          editorium = self.editorium,
                                                          command = self.function,
                                                          defaults = args,
                                                          title = string_helper.capitalise_first_and_fix( self.function.__name__ ),
                                                          options = self.user_options )
            
            if avc is None:
                return None
            
            args = avc.to_argskwargs()
        
        asr = Result( command = self.function,
                      args = args )
        
        self.running_dialogue = FrmMaintenance( parent = self.window,
                                                command = lambda: self.function( *args.args, **args.kwargs ),
                                                auto_close = self.auto_close,
                                                options = self.user_options )
        asr.listen( self.running_dialogue.handle_worker_finished )
        self.running_dialogue.setModal( True )
        self.running_dialogue.show()
        
        try:
            self.bee_thread = _BeeThread( asr, self.running_dialogue )
            self.bee_thread.start()
        except Exception as ex:
            # Thread failed to start (application critical)
            ansi_format_helper.print_traceback( ex )
            raise RuntimeError( "The worker bee thread failed to start. I have logged the causing error to stdout." ) from ex
        
        return asr


class _FnWrapper:
    """
    Wraps a function, we need to do this because QT won't let us send raw
    functions across threads without wrapping them.
    """
    
    
    def __init__( self, fn ) -> None:
        self.__fn = fn
    
    
    def __call__( self, *args, **kwargs ) -> Optional[object]:
        return self.__fn( *args, **kwargs )
    
    
    def __str__( self ) -> str:
        return str( self.__fn )


class _BeeThread( QThread ):
    """
    Thread that runs commands in a "please wait" window.
    """
    __callback = pyqtSignal( _FnWrapper )
    
    
    def __init__( self,
                  async_result: Result,
                  dialogue: FrmMaintenance ):
        ##########################
        # THIS IS THE GUI THREAD #
        ##########################
        QThread.__init__( self )
        self.__callback.connect( self.__invoke_returned )
        self.__async_result = async_result
        self.dialogue: FrmMaintenance = dialogue
    
    
    @override  # QThread
    def run( self ) -> None:
        #############################
        # THIS IS THE WORKER THREAD #
        #############################
        
        #
        # Name the thread
        #
        threading.currentThread().name = "intermake_busybee_running_{}".format( self.__async_result.command.__name__ )
        
        #
        # Execute the command
        #
        self.__async_result.execute( _StdoutRelay( self ) )
        
        if self.__async_result.is_error:
            # Print a message for the debugger
            sys.__stderr__.write( "EXCEPTION FROM COMMAND EXECUTE:\n" )
            sys.__stderr__.write( ansi_format_helper.format_traceback_ex( self.__async_result.exception ) )
            sys.__stderr__.write( "\n" )
        
        #
        # Respond with the result
        #
        self.invoke_in_main_thread( self.__async_result.issue_callbacks )
    
    
    def invoke_in_main_thread( self, where ) -> None:  # WORKER
        """
        Calls "where" back in the main thread
        """
        where = _FnWrapper( where )
        self.__callback.emit( where )  # --> MAIN (via signal)
    
    
    @staticmethod
    def __invoke_returned( where ) -> None:  # <- MAIN (via signal)
        """
        The callback from invoke_in_main_thread - just calls "where".
        """
        where()


class _StdoutRelay:
    def __init__( self, thread: _BeeThread ):
        self.thread = thread
    
    
    def write( self, data: str ):
        if self.thread.dialogue.handle_was_cancelled():
            raise TaskCancelledError()
        
        self.thread.invoke_in_main_thread( lambda: self.thread.dialogue.handle_message_from_worker( data ) )
