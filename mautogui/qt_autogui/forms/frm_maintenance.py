from PyQt5.QtCore import Qt
from PyQt5.QtGui import QCloseEvent, QGuiApplication
from PyQt5.QtWidgets import QDialog, QWidget, QScrollBar
from typing import cast, Callable, Tuple, Dict

from mhelper_qt.qt_gui_helper import exqtSlot

import time
import pyperclip
from mhelper import string_helper
import mhelper_qt as qt
from mautogui.qt_autogui.forms.designer.resource_files import resources_rc
from mautogui.qt_autogui.forms.designer.frm_maintenance_designer import Ui_Dialog
from mautogui.qt_autogui.async_result import Result


__author__ = "Martin Rusilowicz"

cast( None, resources_rc )


class FrmMaintenance( QDialog ):
    """
    This is the "please wait" form that shows when a plugin is running.
    """
    
    
    def __init__( self,
                  parent: QWidget,
                  command: Callable,
                  auto_close: bool,
                  options: dict ):
        """
        CONSTRUCTOR
        """
        QDialog.__init__( self, parent )
        self.ui = Ui_Dialog( self )
        
        # Set our properties
        self.settings = _Options( options )
        self.command = command
        self.__was_cancelled = False
        self.__prefer_auto_close = auto_close
        self.__auto_close_success = self.settings.get( command, "auto_close_on_success", auto_close )
        self.__auto_close_failure = self.settings.get( command, "auto_close_on_failure", auto_close )
        self.__auto_scroll_messages = self.settings.get( command, "auto_scroll_messages", True )
        self.__finished: Result = None
        self.__needs_raise = True
        self.__maximise_progress = False
        self.__maximise_output = False
        self.__has_text_messages = False
        self.__start_time = time.time()
        self.__html=[]
        
        # Set up the window
        self.setWindowTitle( string_helper.capitalise_first_and_fix( command.__name__ ) )
        self.setWindowFlags( Qt.Dialog | Qt.Desktop )
        
        # Hide the close button until the command completes
        self.ui.BTN_CLOSE.setVisible( False )
        
        # Show the "please wait" screen until a message is received
        self.ui.PAGER_MAIN.setCurrentIndex( 0 )
    
    
    def handle_message_from_worker( self, info: str ):
        #
        # Add our message to the textbox
        #
        #
        # Process progress bars separately
        #
        html = qt.qt_gui_helper.ansi_to_html( info )
        self.__html.append( html )
        
        #
        # Update the HTML and scroll into view
        #
        self.ui.TXT_MESSAGES.setHtml( "".join( self.__html ) )
        v: QScrollBar = self.ui.TXT_MESSAGES.verticalScrollBar()
        v.setValue( v.maximum() )
        
        # First message specials
        if self.__needs_raise:
            # Switch to page 1
            self.ui.PAGER_MAIN.setCurrentIndex( 1 )
            
            # Bring the window to the top
            self.activateWindow()
            self.raise_()
            self.__needs_raise = False
    
    
    def handle_was_cancelled( self ) -> bool:
        return self.__was_cancelled
    
    
    def handle_worker_finished( self, asr: Result ):
        #
        # Hold ctrl to prevent auto-close
        #
        keyboard = QGuiApplication.queryKeyboardModifiers()
        
        self.ui.PAGER_MAIN.setCurrentIndex( 1 )
        self.__finished = asr
        
        if asr.is_error:
            self.handle_message_from_worker( "Command finished with errors:\n" )
            self.handle_message_from_worker( "{}".format( asr.exception ) )
            # self.handle_message_from_worker( "<traceback>{}</traceback>".format(  intermake.pr.escape( traceback) ) )
            auto_close = self.__auto_close_failure
        else:
            auto_close = self.__auto_close_success
        
        if (keyboard & Qt.ControlModifier) == Qt.ControlModifier:
            auto_close = False
        
        if auto_close:
            self.close()
        else:
            if asr.is_error:
                self.handle_message_from_worker( "<system>Your query has completed with errors. You may now close the dialogue.</system>" )
            else:
                self.handle_message_from_worker( "<system>Your query has completed. You may now close the dialogue.</system>" )
            
            self.ui.BTN_CANCEL.setVisible( False )
            self.ui.BTN_CLOSE.setVisible( True )
    
    
    def closeEvent( self, event: QCloseEvent ):
        if self.__finished is None:
            event.ignore()
            return
    
    
    @exqtSlot()
    def on_BTN_CANCEL_clicked( self ) -> None:
        """
        Signal handler:
        """
        self.handle_message_from_worker( "~~ Cancel requested ~~ - The process will stop during the next iteration" )
        self.ui.BTN_CANCEL.setVisible( False )
        self.__was_cancelled = True
    
    
    @exqtSlot()
    def on_BTN_CLOSE_clicked( self ) -> None:
        """
        Signal handler:
        """
        pyperclip.copy( "\n".join( self.__html ) )
        self.close()
    
    
    @exqtSlot()
    def on_BTN_OPTIONS_clicked( self ) -> None:
        """
        Signal handler:
        """
        _Menu.show_menu( self.settings, self.command, self )


class _Options:
    def __init__( self, data: dict ):
        self.data = data
    
    
    def key( self, cmd_name, set_name ):
        return "{}.{}".format( cmd_name, set_name )
    
    
    def get( self, cmd_name, set_name, default ):
        return self.data.get( self.key( cmd_name, set_name ), default )
    
    
    def set( self, cmd_name, set_name, value ):
        self.data[self.key( cmd_name, set_name )] = value


class _Menu:
    @staticmethod
    def show_menu( options: _Options, command: Callable, parent_window ):
        mnu_root = qt.QMenu()
        alive = { }
        
        _Menu._mk_options( alive, options, "", mnu_root, "Default settings", True )
        _Menu._mk_options( alive, options, command.__name__, mnu_root, "Settings for “{}”".format( command.__name__ ), False )
        
        r = qt.show_menu( parent_window, mnu_root )
        
        if r is None:
            return
        
        cmd_name, set_name, value = alive[r]
        options.set( cmd_name, set_name, value )
    
    
    @staticmethod
    def _mk_options( alive: dict,
                     options: _Options,
                     cmd_name: str,
                     parent: qt.QMenu,
                     text: str,
                     d: bool ):
        mnu_name = _Menu._mk_menu( alive, parent, text )
        if d:
            #
            # HELP submenu
            #
            mnu_help = _Menu._mk_menu( alive, mnu_name, "Help" )
            _Menu._mk_action( alive, mnu_help, "Show as buttons", options, "", "inline_help", False )
            _Menu._mk_action( alive, mnu_help, "Show as inline text", options, "", "inline_help", True )
        #
        # AUTO-START submenu
        #
        mnu_prompt = _Menu._mk_menu( alive, mnu_name, "Prompt" )
        _Menu._mk_action( alive, mnu_prompt, "Use the default", options, cmd_name, "auto_start_if_parameterless", None )
        _Menu._mk_action( alive, mnu_prompt, "Start running the command", options, cmd_name, "auto_start_if_parameterless", True )
        _Menu._mk_action( alive, mnu_prompt, "Confirm before running the command", options, cmd_name, "auto_start_if_parameterless", False )
        #
        # AUTO-CLOSE submenu 1
        #
        mnu_success = _Menu._mk_menu( alive, mnu_name, "On success" )
        _Menu._mk_action( alive, mnu_success, "Use the default", options, cmd_name, "auto_close_on_success", None )
        _Menu._mk_action( alive, mnu_success, "Automatically close the messages window", options, cmd_name, "auto_close_on_success", True )
        _Menu._mk_action( alive, mnu_success, "Keep the messages window open", options, cmd_name, "auto_close_on_success", False )
        #
        # AUTO-CLOSE submenu 2
        #
        mnu_failure = _Menu._mk_menu( alive, mnu_name, "On failure" )
        _Menu._mk_action( alive, mnu_failure, "Use the default", options, cmd_name, "auto_close_on_failure", None )
        _Menu._mk_action( alive, mnu_failure, "Automatically close the messages window", options, cmd_name, "auto_close_on_failure", True )
        _Menu._mk_action( alive, mnu_failure, "Keep the messages window open", options, cmd_name, "auto_close_on_failure", False )
        #
        # SCROLL-MESSAGES submenu 2
        #
        mnu_scroll = _Menu._mk_menu( alive, mnu_name, "Scroll to new messages" )
        _Menu._mk_action( alive, mnu_scroll, "Use the default", options, cmd_name, "auto_scroll_messages", None )
        _Menu._mk_action( alive, mnu_scroll, "Always", options, cmd_name, "auto_scroll_messages", True )
        _Menu._mk_action( alive, mnu_scroll, "Never", options, cmd_name, "auto_scroll_messages", False )
    
    
    @staticmethod
    def _mk_menu( alive: dict,
                  menu: qt.QMenu,
                  text: str ):
        new_menu = qt.QMenu()
        new_menu.setTitle( text )
        alive[new_menu] = "", "", False
        menu.addMenu( new_menu )
        return new_menu
    
    
    @staticmethod
    def _mk_action( alive: Dict[qt.QObject, Tuple[str, str, object]],
                    menu: qt.QMenu,
                    text: str,
                    options: _Options,
                    cmd_name: str,
                    set_name: str,
                    value: object ):
        action = qt.QAction()
        action.setText( text )
        action.setCheckable( True )
        action.setChecked( options.get( cmd_name, set_name, None ) is value )
        action.setStatusTip( "{}={}".format( cmd_name, set_name ) )
        alive[action] = cmd_name, set_name, value
        menu.addAction( action )
        return action
