from typing import Optional, Dict, Tuple, Callable

from PyQt5.QtWidgets import QDialog, QWidget

from mautogui.qt_autogui.forms.frm_maintenance import _Menu, _Options
from mhelper import string_helper
from mhelper.mannotation import ArgValueCollection, ArgsKwargs, FunctionInspector
import mhelper_qt as qt
from mautogui.qt_autogui.forms.designer.frm_arguments_designer import Ui_Dialog
from mautogui.editoria import qt_editorium


class FrmArguments( QDialog ):
    def __init__( self,
                  parent: QWidget,
                  editorium: qt_editorium.Editorium,
                  command: Callable,
                  defaults: ArgsKwargs,
                  title: str,
                  options: dict ) -> None:
        """
        CONSTRUCTOR
        """
        QDialog.__init__( self, parent )
        
        self.ui = Ui_Dialog( self )
        self.title = title or string_helper.capitalise_first_and_fix( command.__name__ )
        self.setWindowTitle( "{} - {}".format( parent.windowTitle(), self.title ) )
        
        self.__command = command
        
        self.result: ArgValueCollection = None
        
        args = FunctionInspector( command ).args
        self.values = ArgValueCollection( args, read = defaults )
        self.editorium_grid = qt_editorium.EditoriumGrid( grid = self.ui.GRID_ARGS,
                                                          editorium = editorium,
                                                          targets = (self.values,) )
        self.editorium_grid.create_help_button( self.__command.__doc__, self.ui.BTN_HELP_MAIN )
        
        self.options = _Options( options )
        self.__init_controls()
        
        self.ui.LBL_APP_NAME.setText( "editorium" )
    
    
    def __init_controls( self ):
        self.ui.LBL_PLUGIN_NAME.setText( self.title )
        self.editorium_grid.mode = qt_editorium.EditoriumGrid.Layouts.INLINE_HELP if self.options.get( "", "inline_help", False ) else qt_editorium.EditoriumGrid.Layouts.NORMAL
        self.editorium_grid.recreate()
        
        if self.editorium_grid.editor_count == 0:
            label = qt.QLabel()
            label.setText( "There are no user-configurable arguments for this command." )
            label.setSizePolicy( qt.QSizePolicy.Fixed, qt.QSizePolicy.Fixed )
            label.setEnabled( False )
            self.editorium_grid.grid.addWidget( label, 0, 0 )
            self.editorium_grid.grid.update()
    
    
    @staticmethod
    def query( owner_window: QWidget,
               editorium: qt_editorium.Editorium,
               command: Callable,
               defaults: ArgsKwargs,
               title: str,
               options: dict
               ) -> Optional[ArgValueCollection]:
        """
        As `request` but the command is not run when the form closes.
        """
        if defaults is None:
            defaults = ArgsKwargs()
        
        form = FrmArguments( owner_window, editorium, command, defaults, title, options )
        
        if form.exec_():
            return form.result
        else:
            return None
    
    
    @qt.exqtSlot()
    def on_pushButton_clicked( self ) -> None:
        """
        Signal handler:
        """
        
        try:
            self.editorium_grid.commit()
            incomplete = self.values.get_incomplete()
            
            if incomplete:
                raise ValueError( "The following arguments have not been provided:\n{}".format( "\n".join( [("    * " + x) for x in incomplete] ) ) )
            
            self.result = self.values
            
            self.accept()
        except Exception as ex:
            qt.show_exception( self, "Error", ex )
            return
    
    
    @qt.exqtSlot()
    def on_BTN_OPTIONS_clicked( self ) -> None:
        """
        Signal handler:
        """
        inline_help = self.options.get( "", "inline_help", False )
        
        _Menu.show_menu( self.options, self.__command, self )
        
        if inline_help != self.options.get( "", "inline_help", False ):
            self.editorium_grid.commit()
            self.__init_controls()
    
    
    def mk_act( self, name: str, acts: Dict[qt.QAction, Tuple[str, str]], menu: qt.QMenu, state: str ):
        a = qt.QAction()
        a.setText( state )
        acts[a] = name, state
        menu.addAction( a )
    
    
    @qt.exqtSlot()
    def on_BTN_HELP_MAIN_clicked( self ) -> None:
        """
        Signal handler:
        """
        pass
