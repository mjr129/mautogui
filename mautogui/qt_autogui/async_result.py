import sys
from enum import Enum
from typing import List, Optional, Callable
from mhelper.special_types import ArgsKwargs
from mhelper.exception_helper import TracebackCollection
from mhelper.mannotation import ArgValueCollection, FunctionInspector
from mhelper import exception_helper


__author__ = "Martin Rusilowicz"


class TaskCancelledError( Exception ):
    pass


class _EResultState( Enum ):
    PENDING = 0
    SUCCESS = 1
    FAILURE = 2


class ResultRelay:
    def __init__( self, output ):
        self.output = output
        self.history = []
    
    
    def write( self, message ):
        self.history.append( message )
        self.output.write( message )


class Result:
    """
    Intermake asynchronous result container and command executor.
    
    .. note::
    
        * Receiving:
            * All callbacks will be made in the primary thread, i.e. the
              same thread that requested the result.
        * Calling:
            * The `Result` class is not itself thread safe - it is up to
              the host to ensure that `set_result` or `set_error` is
              called in the main thread.
          
    :cvar __INDEX:          Tally on result count.  
    :ivar state:            State of result (pending, success, failure)
    :ivar result:           Result (on success)
    :ivar exception:        Exception (on failure)
    :ivar traceback:        Exception traceback (on failure)
    :ivar messages:         Messages (on success or failure)
    :ivar index:            Index of result (arbitrary)
    :ivar __callbacks:      Result listeners (when pending)
    """
    __INDEX = 0
    __global_callbacks = []
    
    __slots__ = "command", "args", "ui_args", "state", "result", "exception", "traceback", "messages", "index", "__callbacks"
    
    
    def __init__( self,
                  *,
                  command: Callable,
                  args: ArgsKwargs ) -> None:
        """
        Constructs a `Result` in the `PENDING` state.
        """
        Result.__INDEX += 1
        
        self.command: Callable = command
        self.args: ArgsKwargs = args
        self.state = _EResultState.PENDING
        self.result: Optional[object] = None
        self.exception: Exception = None
        self.traceback: Optional[TracebackCollection] = None
        self.messages: Optional[List[str]] = None
        
        self.index = Result.__INDEX
        self.__callbacks: List[Callable[[Result], None]] = []
    
    
    def execute( self, output ) -> None:
        """
        Runs the command.
        
        * This action can be performed in *any thread*.
        * This method may raise any exception.
        * It is the *caller's* responsibility to ensure that `issue_callbacks`
          is called to call the callbacks.
          
        .. warning::
        
            Multi-threading support for `execute` has been removed.
            This method now *reassigns stdout* in a single-threaded fasion.
            **Do not run multiple `executes` in parallel, even from different
            `Result`s**
        """
        if self.state != _EResultState.PENDING:
            raise ValueError( "Cannot execute a task that already has a result." )
        
        # Collect stdout
        relay = ResultRelay( output )
        original = sys.stdout
        sys.stdout = relay
        
        try:
            # Check the argument types are correct
            arguments = FunctionInspector( self.command ).args
            ArgValueCollection( arguments, read = self.args )
            
            # Run the command
            r = self.command( *self.args.args, **self.args.kwargs )
            self.state = _EResultState.SUCCESS
            self.result = r
        except BaseException as ex:
            self.state = _EResultState.FAILURE
            self.exception = ex
            self.traceback = exception_helper.get_traceback_ex( ex )
        finally:
            self.messages = relay.history
            sys.stdout = original
    
    
    def cancel( self, message = None ):
        """
        Cancels the task before it has even begun.
        :param message:     Optional message.
        :return:            Nothing. 
        """
        if self.state != _EResultState.PENDING:
            raise ValueError( "Cannot cancel a task that already has a result." )
        
        message = message or "This task was cancelled before it was initiated."
        self.state = _EResultState.FAILURE
        self.exception = TaskCancelledError( message )
        self.traceback = None
        self.messages = [message]
    
    
    def listen( self, callback: Callable[["Result"], None] ):
        """
        Adds a callback.
        
        This function calls `callback` if the result has completed, otherwise
        `callback` is called when the result completes.
        """
        if self.is_pending:
            self.__callbacks.append( callback )
        else:
            callback( self )
    
    
    @classmethod
    def listen_globally( cls, callback: Callable[["Result"], None] ):
        """
        Adds a callback.
        
        This function calls `callback` if the result has completed, otherwise
        `callback` is called when the result completes.
        """
        cls.__global_callbacks.append( callback )
    
    
    def issue_callbacks( self ):
        """
        Calls the callbacks, including the host itself.
        """
        if self.__callbacks is None:
            raise RuntimeError( "The callbacks have already been called!" )
        
        # Call any global callbacks
        for callback in self.__global_callbacks:
            callback( self )
        
        for callback in self.__callbacks:
            callback( self )
        
        self.__callbacks = None
    
    
    def raise_exception( self ) -> None:
        """
        For a result in the `FAILURE` state, re-raises the exception, otherwise does nothing.
        """
        if self.exception:
            raise self.exception
    
    
    @property
    def is_success( self ) -> bool:
        """
        `True` if the `Result` is in the `SUCCESS` state.
        """
        return self.state == _EResultState.SUCCESS
    
    
    @property
    def is_error( self ) -> bool:
        """
        `True` if the `Result` is in the `ERROR` state.
        """
        return self.state == _EResultState.FAILURE
    
    
    @property
    def is_pending( self ) -> bool:
        """
        `True` if the `Result` is in the `PENDING` state.
        """
        return self.state == _EResultState.PENDING
    
    
    def __repr__( self ):
        """
        Imperfect Python representation.
        """
        if self.is_pending:
            return "{}({}, {}, {}, {})".format( type( self ).__name__, self.index, self.command, repr( self.args ), repr( self.state ) )
        elif self.is_success:
            return "{}({}, {}, {}, {}, result = {})".format( type( self ).__name__, self.index, self.command, repr( self.args ), repr( self.state ), repr( self.result ) )
        else:
            return "{}({}, {}, {}, {}, exception = {})".format( type( self ).__name__, self.index, self.command, repr( self.args ), repr( self.state ), repr( self.exception ) )
    
    
    def __str__( self ) -> str:
        """
        Complete string representation.
        """
        if self.is_pending:
            return "(Pending) {} {}".format( self.command, self.args )
        elif self.is_success:
            return "(Success) {} {} = {}".format( self.command, self.args, self.result )
        else:
            return "(Failure) {} {} = {}".format( self.command, self.args, self.exception )
