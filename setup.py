from distutils.core import setup


setup( name = "mautogui",
       url = "https://bitbucket.org/mjr129/autogui",
       version = "0.0.0.62",
       description = "Automatic UI generation.",
       author = "Martin Rusilowicz",
       license = "https://www.gnu.org/licenses/agpl-3.0.html",
       packages = ["mautogui"
                   ],
       install_requires = ["mhelper",  # MJR, general purpose library
                           "pyperclip",  # Clipboard
                           "PyQt5",  # Ui (GUI)
                           "sip",  # Ui (GUI)
                           ],
       python_requires = ">=3.7",

       classifiers = \
           [
               "Development Status :: 3 - Alpha",
        
               "Environment :: X11 Applications :: Qt",
               "Operating System :: OS Independent",
               "Operating System :: Microsoft :: Windows",
               "Operating System :: MacOS",
               "Operating System :: POSIX :: Linux",
        
               "Topic :: Utilities",
               "Topic :: Multimedia :: Graphics :: Presentation",
               "Topic :: Multimedia :: Graphics :: Viewers",
        
               "License :: OSI Approved :: GNU Affero General Public License v3",
               "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        
               "Natural Language :: English",
               "Programming Language :: Python :: 3.7",
               "Programming Language :: Python :: 3",
               "Programming Language :: Python :: 3 :: Only"
           ]
       )
