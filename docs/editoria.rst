============================
Intermake Autogui - Editoria
============================

.. note::
 
    See the `main page`_ for the primary description of this package.


The `Editoria` packages convert human input to data-types commensurate with `PEP-484`_-style annotations.

For instance, an ``int`` annotation produces an ``int`` result, (showing a ``QSpinBox`` in Qt-mode). An ``Optional[int]`` annotation produces either an ``int`` or a ``None`` result (showing a ``QCheckBox`` and a ``QSpinBox`` in Qt-mode).

There are two sub-packages - the `Qt Editorium`:t: creates `Qt`:t: editors suitable for the provided annotations type, while the `String Editorium` is used where interpreting text alone is useful.

Out of the box, support for the following annotations are included:


**Basic types**

    .. list-table::
        :header-rows: 1
    
        * - `Annotation`
          - `Output`
          - `Editor (Qt only)`
    
        * - ``int``
          - ``int``   
          - ``QSpinBox``
    
        * - ``float``
          - ``float`` 
          - ``QLineEdit``
    
        * - ``str`` 
          - ``str``
          - ``QLineEdit``
    
        * - ``bool``  
          - ``bool``
          - ``QCheckBox`` 

**Typing library**

    .. list-table::
        :header-rows: 1
    
        * - `Annotation`
          - `Output`
          - `Editor (Qt only)`
    
        * - ``Optional[T]``
          - ``T`` | ``None`` 
          - ``QCheckBox`` + ``Editor(T)``
    
        * - ``List[T]``
          - ``list(of T)`` 
          - ``Editor(T)[]``
    
        * - ``Union[...]``
          - ``...``
          - ``Editor(...)[]`` + ``QRadioButton[]``

**Enum library**

    .. list-table::
        :header-rows: 1
    
        * - `Annotation`
          - `Output`
          - `Editor (Qt only)`
    
        * - ``T(Enum)``
          - ``T``  
          - ``QComboBox``
    
        * - ``T(Flag)``
          - ``T``  
          - ``QCheckBox[]``

**Additional annotations**

    .. list-table::
        :header-rows: 1
    
        * - `Annotation`
          - `Description`
          - `Output`
          - `Editor (Qt only)`
    
        * - ``isOptional[T]``
          - An alternative to ``typing.Optional`` for cases where ``T`` is another annotation, rather than a concrete type.
          - ``T`` | ``None`` 
          - ``QCheckBox`` + ``Editor(T)``
    
        * - ``isUnion[...]``
          - An alternative to ``typing.Union`` for cases where ``T`` is another annotation, rather than a concrete type.
          - ``...``
          - ``Editor(...)[]`` + ``QRadioButton[]``
    
        * - ``isFilename``
          - Denotes that the string should be a file path. Parameters on the annotation control mode (open/save) and extension.
          - ``str`` 
          - ``QLineEdit`` + ``QToolButton``
    
        * - ``isDirname``
          - Denotes that the string should be a directory path. Parameters on the annotation control mode (open/save).
          - ``str`` 
          - ``QLineEdit`` + ``QToolButton``
    
        * - ``isOptionList[...]``
          - Denotes that the value can be one of those specified 
          - ``...``
          - ``QComboBox``
    
        * - ``isPassword``
          - Denotes that the value is plain text, but should be obscured 
          - ``str`` 
          - ``QLineEdit``



.. _PEP-484: https://www.python.org/dev/peps/pep-0484/venv
.. _`String Editorium`: str_editorium.rst
.. _`main page`: readme.rst
