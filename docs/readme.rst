====================================================================================================
                                      Intermake Autogui                                       
====================================================================================================

`Intermake Autogui`:t: automatically creates input forms by processing type information gleaned from `PEP-484`_ function annotations, and then calls the functions with STDOUT redirected to a suitable progress window.

The primary export from the library is `WindowedRunner`, which may be called with suitable function and optional default arguments.

Internally, `Intermake Autogui`:t: uses the `autogui.editoria` package to decide which widgets should be chosen to edit a particular type. More information on this package can be found here_.

.. _here: editoria.rst
.. _PEP-484: https://www.python.org/dev/peps/pep-0484/venv